import java.util.Scanner;
//import javax.swing.JOptionPane;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        for (;;) {
            double nadmorskaVisina = sc.nextDouble();
            izpis (nadmorskaVisina, izracunPospeskaNadmorskaVisina (nadmorskaVisina));

        }
        
    }
        
	
    public static void izpis(double nadmorskaVisina, double gravitacijskiPospesek){
        //1.opcija:
        System.out.println(nadmorskaVisina);
        System.out.println(gravitacijskiPospesek);
        
        //2. opcija
        
        //JOptionPane.showMessageDialog(null, "Nadmorska visina je "+nadmorskaVisina, "the tittle", JOptionPane.PLAIN_MESSAGE);
        //JOptionPane.showMessageDialog(null, "Gravitacijski pospesek je "+gravitacijskiPospesek, "the tittle", JOptionPane.PLAIN_MESSAGE);
    }
    
    public static double izracunPospeskaNadmorskaVisina (double visina){
        return (6.674 * 5.972 * Math.pow(10, 13))/Math.pow((6371000 + visina), 2);
    }
}